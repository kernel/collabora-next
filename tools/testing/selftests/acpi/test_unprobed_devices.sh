#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Copyright (c) 2023 Collabora Ltd
#
# Inspired by the tools/testing/selftests/dt/test_unprobed_devices.sh
# script, adapted for the ACPI use case.
#
# This script checks whether devices declared in the ACPI namespace and
# supported by the kernel are correctly bound to a driver.
#
# To do this, two lists are used:
# * a list of ACPI IDs matched by existing drivers
# * a list of IDs that should be ignored
#

DIR="$(dirname "$(readlink -f "$0")")"

KTAP_HELPERS="${DIR}/../kselftest/ktap_helpers.sh"
if ! source "$KTAP_HELPERS"; then
	exit 4
fi

ACPI_SYSTEM_DIR="/sys/devices/LNXSYSTM:00"
ID_IGNORE_LIST="${DIR}"/id_ignore_list
ID_LIST="${DIR}"/id_list

PCI_CLASS_BRIDGE_HOST="0x0600"
PCI_CLASS_BRIDGE_ISA="0x0601"

ktap_print_header

if [[ ! -d "${ACPI_SYSTEM_DIR}" ]]; then
	ktap_skip_all "${ACPI_SYSTEM_DIR} doesn't exist."
	exit "${KSFT_SKIP}"
fi

# The ACPI specification mandates that ACPI objects representing devices on
# non-enumerable and enumerable busses contain a _HID or an _ADR
# identification object respectively. Get a list of devices of both types,
# by searching the ACPI sysfs subtree for directories containing a hid or
# adr attribute.
supp_dev_paths=$(while IFS=$'\n' read -r dev_path; do
	if [ ! -f "${dev_path}"/hid ] && [ ! -f "${dev_path}"/adr ]; then
		continue
	fi

	# Check if the device is present, enabled, and functioning properly
	status="${dev_path}/status"
	if [ -f "${status}" ]; then
		status_hex=$(($(cat "${status}")))

		if [ $((status_hex & 1)) -eq 0 ] ||
			[ $((status_hex >> 1 & 1)) -eq 0 ] ||
			[ $((status_hex >> 3 & 1)) -eq 0 ]; then
			continue
		fi
	fi

	if [ -n "$(find -L "${dev_path}" -maxdepth 1 -name "physical_node*" -print -quit)" ]; then
		for node in "${dev_path}"/physical_node*; do
			# Ignore devices without a subsystem, devices that link to
			# other devices, and class devices
			if [ ! -d "${node}/subsystem" ] ||
				[ -d "${node}/device" ] ||
				[[ "$(readlink -f "${node}/subsystem")" == /sys/class/* ]]; then
				continue
			fi

			echo "${node}"
		done
	fi
done < <(find ${ACPI_SYSTEM_DIR} -name uevent -exec dirname {} \;))

supp_dev_paths_num=$(echo "${supp_dev_paths}" | wc -w)
ktap_set_plan "${supp_dev_paths_num}"

# Iterate over ACPI devices
for dev_path in ${supp_dev_paths}; do
	if [ -f "${dev_path}/firmware_node/path" ]; then
		acpi_path="$(<"${dev_path}"/firmware_node/path)"
	fi

	dev_link=$(readlink -f "${dev_path}")
	desc="${acpi_path}-${dev_link#/sys/devices/}"

	if [ -f "${dev_path}/firmware_node/hid" ]; then
		hid="$(<"${dev_path}"/firmware_node/hid)"

		if [ -f "${dev_path}/firmware_node/modalias" ]; then
			modalias=$(<"${dev_path}/firmware_node/modalias")
			cid=$(echo "${modalias}" | cut -d':' -f3)

			# Skip devices with ignored HID/CID
			if ignored_id=$(grep -i "${hid}" "${ID_IGNORE_LIST}" ||
				{ [ -n "${cid}" ] && grep -i "${cid}" "${ID_IGNORE_LIST}"; }); then
				ktap_print_msg "ID ${ignored_id} ignored [SKIP]"
				ktap_test_skip "${desc}"
				continue
			fi
			# Skip devices with unsupported HID/CID
			if [[ "${hid}" != LNX* ]] && ! grep -x -q -i "${hid}" "${ID_LIST}"; then
				if [ -z "${cid}" ] || ! grep -x -q -i "${cid}" "${ID_LIST}"; then
					ktap_print_msg "no match for ${hid}${cid:+:${cid}} found \
						in the supported IDs list [SKIP]"
					ktap_test_skip "${desc}"
					continue
				fi
			fi
		fi
	fi

	# Skip bridges that don't require a driver
	if [ -f "${dev_path}/class" ]; then
		class=$(<"${dev_path}"/class)
		if [[ ${class} == ${PCI_CLASS_BRIDGE_HOST}* ]] ||
			[[ ${class} == ${PCI_CLASS_BRIDGE_ISA}* ]]; then
			ktap_print_msg "device linked to ${desc} does not require a driver [SKIP]"
			ktap_test_skip "${desc}"
			continue
		fi
	fi

	# Search for the driver in both the device folder and the companion's folder
	if [ -d "${dev_path}/driver" ] || [ -d "${dev_path}/firmware_node/driver" ]; then
		ktap_test_pass "${desc}"
	# Skip char devices
	elif [ -f "${dev_path}/dev" ]; then
		ktap_print_msg "${desc} is a char device [SKIP]"
		ktap_test_skip "${desc}"
		continue
	else
		ktap_test_fail "${desc}"
	fi

done

ktap_finished
